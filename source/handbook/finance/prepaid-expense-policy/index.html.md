---
layout: markdown_page
title: “Prepaid Expense Policy”
---

## Prepaid Expense Policy

### Purpose
This policy describes the methodology used to monitor and account for GitLab’s prepaid expenses.

### Prepaid Expenses Defined
A [*Prepaid Expense*](https://www.investopedia.com/terms/p/prepaidexpense.asp?ad=dirN&qo=investopediaSiteSearch&qsrc=0&o=40186) arises when a cash disbursement is made for goods and services prior to realizing the associated benefits of the underlying goods and services. These transactions are recorded as assets until the goods and services are realized, at which point an expense is recorded. Our minimum threshold for recording prepaid expenses is [$1,000 USD](/handbook/people-operations/global-compensation/#exchange-rates). Anything under this amount is expensed immediately.

### Identification and Recording of Prepaid Expenses 
Once a purchase request makes it through the [company approval workflow](/handbook/finance/procure-to-pay/), Finance will take the following steps to ensure prepaid expenses are recorded accurately: 

1. The Accounts Payable Administrator flags all bills that qualify as prepaid expenses during the normal course of processing bills in the AP mailbox.

1. The flagged bills are then analyzed and added to the asset register located in Google drive. Information includes expense category, department, benefit period, and amount to be amortized. 

1. At the end of each month, the Accountant reconciles the prepaid additions to the general ledger, which includes verifying amortization schedules and amounts.  

Amortization is recorded straight line based on a mid-month amortization method as follows:
If the first month of service begins on the 1st to the 15th of the month, a full month amortization will be recorded in the current month.  If the first month of service begins on the 16th to the last day of the month, amortization will begin on the 1st day of the subsequent month.

Mid-Month Amortization Method does not apply to prepaid expenses with a monthly amortization equal to or greater than 50,000 USD or if the amortization if spread only over 1 period.  If monthly amortization is equal to or more than 50,000 USD, the first month amortization will be calculated based on actual number of days where services were rendered.

Prepaid Not Paid:  For any prepaid expenses not processed for payment, an adjustment for "prepaid not paid" is posted to the respective prepaid expense account and AP manual adjusment account (GL Account 2001).  A prepaid expense is not treated as an asset if a liability remains in the AP sub-ledger.  Prepaid not paid adjusments are performed on a quartly basis at minimum.
Any deposits paid which will be held for more than 12 months such as security deposits or deposits to retain consultants will be booked to Security & Other Deposits (GL Account 1620)

Prepaid Bonuses with a Clawback will be recoreded to Prepaid Bonus w/Clawback (GL Account 1152) and will be amortized in accordance with the bonus agreement terms, using the mid-month convention.

4. Finally, the balance is reviewed one last time when the Controller performs a review of the financials prior to closing the period.

### Contribute related expenses
Team member travel expenses are expensed in the period incurred. Costs related to third party vendors such as hotels, facilities, excursions are recorded to prepaid expenses and recognized as expense at the time of the event.

